# Contributing

Thank you for considering to add to the Galvanize Authentication API library.
This document is about how to set up your environment, and the GitLab workflow
that you should use during your development.

## Setting up your development environment

This is a Python library that is publicly distributed on PyPi.org with
supporting documentation on ReadTheDocs.io. To ease this development,
we use Poetry as a way to manage our dependencies, test environments,
and publishing the library.

You can participate in this project using Linux, macOS, or WSL2 on Windows.

Get your development environment set up with these steps:

1. Install Python 3.9 on your computer using whatever method you'd like.
   We recommend using [pyenv](https://github.com/pyenv/pyenv).
1. Install [Poetry](https://python-poetry.org/) using the appropriate
   installation method for your environment. After installation, configure
   Poetry as you'd like, but make sure you set the following
   [configuration settings](https://python-poetry.org/docs/configuration/):

   * `virtualenvs.in-project = true`
   * `virtualenvs.prefer-active-python = true`

1. Yeah, clone this thing...
1. In the project's root directory, run `poetry install`
1. Make sure your text editor has [`mypy`](https://www.mypy-lang.org/)
   support so that you get the linting feedback while you're writing.
   If you use Visual Studio Code with the Pyhton + PyLance plugin, you'll
   be set because the `.vscode/` contains the settings for `mypy` and
   [`black`](https://github.com/psf/black), the formatter.

## Contributing workflow

Our workflow uses and adaptation of the
[GitLab Merge Request workflow](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html). 

1. Find an [Issue](https://gitlab.com/galvanize-inc/foss/auth-api/-/issues) that
   you'd like to work on.
1. Change the status of the issue using one of the two following methods:
   
   * Move the issue from the "Open" lane into the "Status: In Progress" lane on the
     [Development Issue Board](https://gitlab.com/galvanize-inc/foss/auth-api/-/boards)
   * Add the "Status: In Progress" label to the issue

1. Use the "Create merge request" button to create a merge request and a branch for the
   issue.

   * Leave the merge request in _Draft_ status
   * Leave the message "Closes #n" in the _Description_
   * Assign the merge request to yourself
   * If you are working on the issue with another person, please put the other person's
     GitLab handle in the _Description_ field

1. Perform a Git `fetch` to get the new branch in your local repository. If your remote
   is named `origin`, then you can use the command

   ```sh
   git fetch origin
   ```

1. Check out the branch in your local repository that was created in the second step.
1. Do your work.

   * Write unit tests when they make sense.
   * Commit often using the _Commit message guidelines_ below.
   * Push when you feel things are okay. Check that you didn't break the build. If you
     did, fix it.
   * If you have thoughts or questions, you can communicate with the maintainers through
     the commenting mechanism on the **issue** (not the merge request).

1. When you're ready for review, please complete the following steps:

   1. Change the status of the issue from "Status: In Progress" to "Status: In Review"
      using the Development Issue Board, or by removing the "Status: In Progress" label
      and adding the "Status: In Review" label
   1. Add an appropriate _Reviewer_ to the merge request to notify the reviewer that the
      merge request is ready for review

1. The reviewer will provide feedback to you. If everything is okay, they will merge
   your change and move the issue to the Closed state. If the reviewer finds actionable
   items for you to complete, then they will move the issue back to "Status: In Progress"
   and remove themself as the reviewer.

Thank you for following this workflow! It makes asynchronous work possible through the
collaboration mechanism of GitLab and its tools.

## Commit message guidelines

Commit messages should follow the guidelines below, for reasons explained by Chris
Beams in [How to Write a Git Commit Message](https://cbea.ms/git-commit/):

* The commit subject and body must be separated by a blank line.
* The commit subject must start with a capital letter.
* The commit subject must not be longer than 72 characters.
* The commit subject must not end with a period.
* The commit body must not contain more than 72 characters per line.
* The commit subject or body must not contain Emojis.
* Commits that change 30 or more lines across at least 3 files should describe
  these changes in the commit body.
* Use issues, milestones, and merge requests’ full URLs instead of short
  references, as they are displayed as plain text outside of GitLab.
* The merge request should not contain more than 10 commit messages.
* The commit subject should contain at least 3 words.
