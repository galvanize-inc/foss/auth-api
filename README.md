# Galvanize Authtentication API

This is the official Python library for Galvanize, Inc.'s authentication API.

Please read the
[documentation](https://galvanize-authentication-api.readthedocs.io/en/latest/intro.html)
to use this project.

## Developing

Development on this project is limited to employees,
contractors, and students of Galvanize, Inc.

